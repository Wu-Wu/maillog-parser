#!/bin/sh

CMD=${1:-app}
RUNNING_ENV=${MOJO_ENV:-development}

if [ -f /app_from_host/entry.sh ]; then
    echo "itone[${CMD}]: running code from local mount"
    BASE_DIR=/app_from_host
else
    echo "itone[${CMD}]: running code from image"
    BASE_DIR=/app
fi

cd ${BASE_DIR}

shift

case $CMD in
    app)
        echo "[app] args {$@}"
        if [ "${RUNNING_ENV}" != "production" ]; then
            exec morbo -w lib -l http://*:5000 app.pl
        else
            exec hypnotoad -f app.pl
        fi
        break
        ;;
    *)
        echo "[echo] args {$@}"
        ;;
esac
