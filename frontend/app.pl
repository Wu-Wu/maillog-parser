#!/usr/bin/env perl

use strict;
use warnings;
use lib qw( lib );

use Mojolicious::Lite;

unshift @{ app->plugins->namespaces }, 'ITOne::Plugin';

app->plugin( 'Common' );

# поиск информации в базе
post('/search')->to( 'root#search' );
# статистика количества записей в таблицах
post('/stats')->to( 'root#stats' );
# парсинг лога и загрузка информации
post('/parse')->to( 'root#parse' );
# очистка базы от записей
post('/truncate')->to( 'root#truncate' );

app->start;
