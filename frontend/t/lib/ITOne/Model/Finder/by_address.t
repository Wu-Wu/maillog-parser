#!/usr/bin/env perl

use utf8;
use Test::Spec;

use ITOne::Model::Finder;

describe "by_address" => sub {
    my ( $model, $db );

    before all => sub {
        $db = stub(
            selectall_arrayref => [],
            selectrow_array    => 42,
        );

        $model = ITOne::Model::Finder->new( db => $db );
    };

    describe "always" => sub {
        it "should invoke db instance" => sub {
            $model->expects('db')->returns($db)->exactly(2);

            $model->by_address( 'foo@example.com', 0, 10 );

            pass;
        };

        it "should invoke selectall_arrayref" => sub {
            $db->expects('selectall_arrayref')->returns([]);

            $model->by_address( 'foo@example.com', 0, 10 );

            pass;
        };

        it "should invoke selectrow_array" => sub {
            $db->expects('selectrow_array')->returns(0);

            $model->by_address( 'foo@example.com', 0, 10 );

            pass;
        };

        it "should return results" => sub {
            cmp_deeply
                [ $model->by_address( 'foo@example.com', 0, 10 ) ],
                [ 42, [] ];
        };
    };

    describe "get entries" => sub {
        it "should match query" => sub {
            $db->expects('selectall_arrayref')->returns(sub {
                my $query = $_[1];

                $query =~ s/[\n\r]//g;
                $query =~ s/\s+/ /g;
                $query =~ s/^\s*//;
                $query =~ s/\s*$//;

                is $query, q{SELECT SQL_CALC_FOUND_ROWS m.created_at AS timestamp, l.internal_id, l.logline FROM logs l JOIN messages m ON l.internal_id = m.internal_id WHERE l.address = ? ORDER BY l.internal_id, l.created_at LIMIT ?};
                return [];
            });

            $model->by_address( 'foo@example.com', 0, 10 );
        };

        it "should match attrs" => sub {
            $db->expects('selectall_arrayref')->returns(sub {
                cmp_deeply
                    $_[2],
                    {
                        Slice => {}
                    };
                return [];
            });

            $model->by_address( 'foo@example.com', 0, 10 );
        };

        it "should match bind values" => sub {
            $db->expects('selectall_arrayref')->returns(sub {
                my ( undef, undef, undef, @binds ) = @_;
                cmp_deeply
                    \@binds,
                    [ 'foo@example.com', 10 ];
                return [];
            });

            $model->by_address( 'foo@example.com', 0, 10 );
        };
    };

    describe "get total" => sub {
        it "should match query" => sub {
            $db->expects('selectrow_array')->returns(sub {
                my $query = $_[1];

                is $query, q{SELECT FOUND_ROWS()};
                return 0;
            });

            $model->by_address( 'foo@example.com', 0, 10 );
        };
    };
};

runtests;
