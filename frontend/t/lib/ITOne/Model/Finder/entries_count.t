#!/usr/bin/env perl

use utf8;
use Test::Spec;

use Mojo::JSON qw( true false );

use ITOne::Model::Finder;

describe "entries_count" => sub {
    my ( $model, $db );

    before all => sub {
        $db = stub(
            selectrow_array => 42,
        );

        $model = ITOne::Model::Finder->new( db => $db );
    };

    describe "always" => sub {
        it "should invoke db instance" => sub {
            $model->expects('db')->returns($db)->exactly(2);

            $model->entries_count();

            pass;
        };

        it "should invoke selectrow_array" => sub {
            $db->expects('selectrow_array')->returns(0)->exactly(2);

            $model->entries_count();

            pass;
        };
    };

    describe "get data" => sub {
        it "should match queries" => sub {
            my $queries = [];

            $db->expects('selectrow_array')->returns(sub {
                my $query = $_[1];

                $query =~ s/[\n\r]//g;
                $query =~ s/\s+/ /g;
                $query =~ s/^\s*//;
                $query =~ s/\s*$//;

                push @$queries, $query;

                return 0;
            })->exactly(2);

            $model->entries_count();

            cmp_deeply
                $queries,
                [
                    q{SELECT COUNT(*) FROM messages},
                    q{SELECT COUNT(*) FROM logs},
                ];
        };
    };

    describe "results" => sub {
        it "should always be" => sub {
            cmp_deeply
                $model->entries_count(),
                superhashof({
                    success  => true,
                    messages => 42,
                    logs     => 42,
                });
        };

        it "should set broken to 'false' (no messages)" => sub {
            my @rows = ( 0, 42 );

            $db->expects('selectrow_array')->returns(sub { shift(@rows) })->exactly(2);

            cmp_deeply
                $model->entries_count(),
                superhashof({
                    broken   => true,
                });
        };

        it "should set broken to 'false' (no logs)" => sub {
            my @rows = ( 42, 0 );

            $db->expects('selectrow_array')->returns(sub { shift(@rows) })->exactly(2);

            cmp_deeply
                $model->entries_count(),
                superhashof({
                    broken => true,
                });
        };

        it "should set broken to 'true' otherwise" => sub {
            my @rows = ( 42, 42 );

            $db->expects('selectrow_array')->returns(sub { shift(@rows) })->exactly(2);

            cmp_deeply
                $model->entries_count(),
                superhashof({
                    broken => false,
                });
        };
    };
};

runtests;
