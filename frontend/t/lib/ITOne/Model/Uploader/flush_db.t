#!/usr/bin/env perl

use utf8;
use Test::Spec;

use Mojo::JSON qw( true );

use ITOne::Model::Uploader;

describe "flush_db" => sub {
    my ( $model, $db );

    before all => sub {
        $db = stub(
            do => 1,
        );

        $model = ITOne::Model::Uploader->new( db => $db );
    };

    describe "always" => sub {
        it "should invoke db instance" => sub {
            $model->expects('db')->returns($db)->exactly(2);

            $model->flush_db();

            pass;
        };

        it "should invoke do" => sub {
            $db->expects('do')->returns(0)->exactly(2);

            $model->flush_db();

            pass;
        };
    };

    describe "get data" => sub {
        it "should match queries" => sub {
            my $queries = [];

            $db->expects('do')->returns(sub {
                my $query = $_[1];

                $query =~ s/[\n\r]//g;
                $query =~ s/\s+/ /g;
                $query =~ s/^\s*//;
                $query =~ s/\s*$//;

                push @$queries, $query;

                return 0;
            })->exactly(2);

            $model->flush_db();

            cmp_deeply
                $queries,
                [
                    q{DELETE FROM messages},
                    q{DELETE FROM logs},
                ];
        };
    };

    describe "results" => sub {
        it "should always be" => sub {
            cmp_deeply
                $model->flush_db(),
                superhashof({
                    success => true,
                });
        };

        it "should return number affected rows" => sub {
            my @rows = ( 37, 42 );

            $db->expects('do')->returns(sub { shift(@rows) })->exactly(2);

            cmp_deeply
                $model->flush_db(),
                superhashof({
                    messages => 37,
                    logs     => 42,
                });
        };
    };
};

runtests;
