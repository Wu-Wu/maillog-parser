#!/usr/bin/env perl

use utf8;
use Test::Spec;

use ITOne::Model::Uploader;

describe "insert_entry" => sub {
    my ( $model, $sth, $entry );

    before all => sub {
        $sth = stub(
            execute => 1,
            rows    => 1,
        );

        $model = ITOne::Model::Uploader->new;

        $model->stubs( stmt_handle => $sth );
    };

    before each => sub {
        $entry = {
            destination => 'logs',
            created_at  => '2021-11-25 19:35:11',
            internal_id => '1Rwtcq-0000Ac-5W',
            logline     => 'grereyjbpddf@gmail.com R=dnslookup T=remote_smtp ...',
            address     => 'grereyjbpddf@gmail.com',
        };
    };

    it "should get statement" => sub {
        $model->expects('stmt_handle')->returns(sub {
            is $_[1], 'logs';
            return $sth;
        });

        $model->insert_entry( $entry );
    };

    it "should execute statement" => sub {
        $sth->expects('execute')->returns(sub {
            shift;
            cmp_deeply
                [ @_ ],
                [
                    '2021-11-25 19:35:11',
                    '1Rwtcq-0000Ac-5W',
                    'grereyjbpddf@gmail.com R=dnslookup T=remote_smtp ...',
                    'grereyjbpddf@gmail.com',
                ];
        });

        $model->insert_entry( $entry );
    };

    it "should get affected rows" => sub {
        $sth->expects('rows')->returns(1);

        $model->insert_entry( $entry );

        pass;
    };

    it "should return result" => sub {
        cmp_deeply
            [ $model->insert_entry( $entry ) ],
            [ 'logs', 1 ];
    };
};

runtests;
