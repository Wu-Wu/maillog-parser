#!/usr/bin/env perl

use utf8;
use Test::Spec;
use Test::Exception;

use ITOne::Model::Uploader;

describe "stmt_handle" => sub {
    my ( $model, $db );

    before all => sub {
        $db = stub( prepare => 1 );
    };

    before each => sub {
        $model = ITOne::Model::Uploader->new( db => $db );
    };

    it "should prepare statement for 'messages'" => sub {
        $db->expects('prepare')->returns(sub {
            my $query = $_[1];

            $query =~ s/[\n\r]//g;
            $query =~ s/\s+/ /g;
            $query =~ s/^\s*//;
            $query =~ s/\s*$//;

            is $query, q{INSERT INTO messages (created_at, internal_id, message_id, logline) VALUES (?, ?, ?, ?)};
            return 1;
        });

        $model->stmt_handle( 'messages' );
    };

    it "should return cached statement for 'messages'" => sub {
        $db->expects('prepare')->never;

        $model->stmt_handle( 'messages' );

        pass;
    };

    it "should prepare statement for 'logs'" => sub {
        $db->expects('prepare')->returns(sub {
            my $query = $_[1];

            $query =~ s/[\n\r]//g;
            $query =~ s/\s+/ /g;
            $query =~ s/^\s*//;
            $query =~ s/\s*$//;

            is $query, q{INSERT INTO logs (created_at, internal_id, logline, address) VALUES (?, ?, ?, ?)};
            return 1;
        });

        $model->stmt_handle( 'logs' );
    };

    it "should return cached statement for 'logs'" => sub {
        $db->expects('prepare')->never;

        $model->stmt_handle( 'logs' );

        pass;
    };

    it "should throws on invalid statement name" => sub {
        throws_ok
            sub { $model->stmt_handle( 'foo' ) },
            qr/^Invalid handle name: 'foo'/,
            '';
    };

    it "should lives on 'messages'" => sub {
        lives_ok
            sub { $model->stmt_handle( 'messages' ) },
            '';
    };

    it "should lives on 'logs'" => sub {
        lives_ok
            sub { $model->stmt_handle( 'logs' ) },
            '';
    };
};

runtests;
