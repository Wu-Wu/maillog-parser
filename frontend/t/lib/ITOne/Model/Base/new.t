#!/usr/bin/env perl

use utf8;
use lib qw( /app/lib /app_from_host/lib );
use Test::Spec;

use ITOne::Model::Base;

describe "new" => sub {
    it "should create new instance" => sub {
        my $m = ITOne::Model::Base->new;

        cmp_deeply
            $m,
            all(
                Isa('ITOne::Model::Base'),
                methods( db => undef ),
            );
    };

    it "should inject params" => sub {
        my $m = ITOne::Model::Base->new(
            foo => 1,
            bar => 'quux',
            db  => 42,
        );

        cmp_deeply
            $m,
            noclass(superhashof({
                foo => 1,
                bar => 'quux',
            }));
    };

    it "should update accessor data" => sub {
        my $m = ITOne::Model::Base->new;

        my $old = $m->db;

        $m->db(42);

        cmp_deeply
            [ $old, $m->db ],
            [ undef, 42 ];
    };
};

runtests;
