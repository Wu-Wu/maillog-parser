#!/usr/bin/env perl

use utf8;
use Test::Spec;

use ITOne::Parser;

describe "tokenize" => sub {
    my ( $fn );

    before all => sub {
        $fn = \&ITOne::Parser::tokenize;
    };

    it "should return data (<=)" => sub {
        my $line = q{tpxmuwr@somehost.ru H=mail.somehost.com [84.154.134.45] P=esmtp S=1289 id=120213143629.COM_FM_END.205359@whois.somehost.ru};

        cmp_deeply
            $fn->( $line ),
            [
                'tpxmuwr@somehost.ru',
                'H',
                'mail.somehost.com [84.154.134.45]',
                'P',
                'esmtp',
                'S',
                1289,
                'id',
                '120213143629.COM_FM_END.205359@whois.somehost.ru',
            ];
    };

    it "should return data (=>)" => sub {
        my $line = q{:blackhole: <tpxmuwr@somehost.ru> R=blackhole_router};

        cmp_deeply
            $fn->( $line ),
            [
                ':blackhole: <tpxmuwr@somehost.ru>',
                'R',
                'blackhole_router',
            ];
    };

    it "should return data (->)" => sub {
        my $line = q{ahuifeum@inbox.ru R=dnslookup T=remote_smtp H=mxs.mail.ru [94.100.176.20]* C="250 OK id=1RwtKE-00011s-D0"};

        cmp_deeply
            $fn->( $line ),
            [
                'ahuifeum@inbox.ru',
                'R',
                'dnslookup',
                'T',
                'remote_smtp',
                'H',
                'mxs.mail.ru [94.100.176.20]*',
                'C',
                '"250 OK',
                'id',
                '1RwtKE-00011s-D0"',
            ];
    };

    it "should return data (==)" => sub {
        my $line = q{grereyjbpddf@gmail.com R=dnslookup T=remote_smtp defer (-1): domain matches queue_smtp_domains, or -odqs set};

        cmp_deeply
            $fn->( $line ),
            [
                'grereyjbpddf@gmail.com',
                'R',
                'dnslookup',
                'T',
                'remote_smtp defer (-1): domain matches queue_smtp_domains, or -odqs',
                'set',
            ];
    };

    it "should return data (**)" => sub {
        my $line = q{wxvparobkymnbyemevz@london.com: retry timeout exceeded};

        cmp_deeply
            $fn->( $line ),
            [
                'wxvparobkymnbyemevz@london.com: retry timeout',
                'exceeded',
            ];
    };
};


runtests;
