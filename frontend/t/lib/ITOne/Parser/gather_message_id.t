#!/usr/bin/env perl

use utf8;
use Test::Spec;

use ITOne::Parser;

describe "gather_message_id" => sub {
    my ( $parser );

    before all => sub {
        $parser = ITOne::Parser->new;
    };

    it "should return data" => sub {
        my $line = q{tpxmuwr@somehost.ru H=mail.somehost.com [84.154.134.45] P=esmtp S=1289 id=120213143629.COM_FM_END.205359@whois.somehost.ru};

        cmp_deeply
            { $parser->gather_message_id( '<=', $line ) },
            {
                message_id => '120213143629.COM_FM_END.205359@whois.somehost.ru',
            };
    };

    it "should not return data" => sub {
        my $line = q{:blackhole: <tpxmuwr@somehost.ru> R=blackhole_router};

        ok !$parser->gather_message_id( '<=', $line );
    };
};

runtests;
