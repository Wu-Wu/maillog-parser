#!/usr/bin/env perl

use utf8;
use Test::Spec;

use ITOne::Parser;

describe "gather_address" => sub {
    my ( $parser );

    before all => sub {
        $parser = ITOne::Parser->new;
    };

    it "should return data" => sub {
        my $line = q{grereyjbpddf@gmail.com R=dnslookup T=remote_smtp defer (-1): domain matches queue_smtp_domains, or -odqs set};

        cmp_deeply
            { $parser->gather_address( '==', $line ) },
            {
                address => 'grereyjbpddf@gmail.com',
            };
    };

    it "should extract and return data" => sub {
        my $line = q{:blackhole: <tpxmuwr@somehost.ru> R=blackhole_router};

        cmp_deeply
            { $parser->gather_address( '=>', $line ) },
            {
                address => 'tpxmuwr@somehost.ru',
            };
    };
};

runtests;
