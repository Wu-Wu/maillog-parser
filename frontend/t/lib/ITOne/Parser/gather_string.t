#!/usr/bin/env perl

use utf8;
use Test::Spec;

use ITOne::Parser;

describe "gather_string" => sub {
    my ( $parser );

    before all => sub {
        $parser = ITOne::Parser->new;
    };

    it "should return data" => sub {
        my $line = q{agugu.ru [193.232.158.145] Operation timed out};

        cmp_deeply
            { $parser->gather_string( $line ) },
            {
                address => '',
                logline => $line,
            };
    };
};

runtests;
