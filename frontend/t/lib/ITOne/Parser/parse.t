#!/usr/bin/env perl

use utf8;
use Test::Spec;

use ITOne::Parser;

describe "parse" => sub {
    my ( $parser );

    before all => sub {
        $parser = ITOne::Parser->new;
    };

    it "should return false when no line" => sub {
        ok !$parser->parse();
    };

    it "should accept flagged '<='" => sub {
        my $line = q{2012-02-13 14:39:22 1RwtJa-000AFB-07 => :blackhole: <tpxmuwr@somehost.ru> R=blackhole_router};

        cmp_deeply
            $parser->parse( $line ),
            {
                destination => 'logs',
                created_at  => '2012-02-13 14:39:22',
                internal_id => '1RwtJa-000AFB-07',
                logline     => q{:blackhole: <tpxmuwr@somehost.ru> R=blackhole_router},
                address     => 'tpxmuwr@somehost.ru',
            };
    };

    it "should accept flagged '=='" => sub {
        my $line = q{2012-02-13 14:39:22 1RookS-000Pg8-VO == udbbwscdnbegrmloghuf@london.com R=dnslookup T=remote_smtp};

        cmp_deeply
            $parser->parse( $line ),
            {
                destination => 'logs',
                created_at  => '2012-02-13 14:39:22',
                internal_id => '1RookS-000Pg8-VO',
                logline     => q{udbbwscdnbegrmloghuf@london.com R=dnslookup T=remote_smtp},
                address     => 'udbbwscdnbegrmloghuf@london.com',
            };
    };

    it "should accept flagged '<='" => sub {
        my $line = q{2012-02-13 14:39:22 1RwtJa-0009RI-2d <= tpxmuwr@somehost.ru H=mail.somehost.com [84.154.134.45] id=120213143629.COM_FM_END.205359@whois.somehost.ru};

        cmp_deeply
            $parser->parse( $line ),
            {
                destination => 'messages',
                created_at  => '2012-02-13 14:39:22',
                internal_id => '1RwtJa-0009RI-2d',
                logline     => q{tpxmuwr@somehost.ru H=mail.somehost.com [84.154.134.45] id=120213143629.COM_FM_END.205359@whois.somehost.ru},
                message_id  => '120213143629.COM_FM_END.205359@whois.somehost.ru',
            };
    };

    it "should accept flagged '->'" => sub {
        my $line = q{2012-02-13 15:11:45 1RwtmW-000MsX-Aw -> fwjatugliro@gmail.com R=dnslookup T=remote_smtp};

        cmp_deeply
            $parser->parse( $line ),
            {
                destination => 'logs',
                created_at  => '2012-02-13 15:11:45',
                internal_id => '1RwtmW-000MsX-Aw',
                logline     => q{fwjatugliro@gmail.com R=dnslookup T=remote_smtp},
                address     => 'fwjatugliro@gmail.com',
            };
    };

    it "should accept flagged '**'" => sub {
        my $line = q{2012-02-13 15:11:41 1RwtmW-000MsX-CK ** bswdhpjxorekjaelb@gmail.com R=dnslookup T=remote_smtp};

        cmp_deeply
            $parser->parse( $line ),
            {
                destination => 'logs',
                created_at  => '2012-02-13 15:11:41',
                internal_id => '1RwtmW-000MsX-CK',
                logline     => q{bswdhpjxorekjaelb@gmail.com R=dnslookup T=remote_smtp},
                address     => 'bswdhpjxorekjaelb@gmail.com',
            };
    };

    it "should accept unflagged" => sub {
        my $line = q{2012-02-13 14:39:22 1RwtJa-000AFB-07 Completed};

        cmp_deeply
            $parser->parse( $line ),
            {
                destination => 'logs',
                created_at  => '2012-02-13 14:39:22',
                internal_id => '1RwtJa-000AFB-07',
                logline     => 'Completed',
                address     => '',
            };
    };
};

runtests;
