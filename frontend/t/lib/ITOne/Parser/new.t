#!/usr/bin/env perl

use utf8;
use Test::Spec;

use ITOne::Parser;

describe "new" => sub {
    it "should create new instance" => sub {
        my $m = ITOne::Parser->new;

        cmp_deeply
            $m,
            all(
                Isa('ITOne::Parser'),
                methods(
                    parse             => ignore(),
                    gather_message_id => ignore(),
                    gather_address    => ignore(),
                    gather_string     => ignore(),
                ),
            );
    };
};

runtests;
