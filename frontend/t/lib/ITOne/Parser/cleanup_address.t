#!/usr/bin/env perl

use utf8;
use Test::Spec;

use ITOne::Parser;

describe "cleanup_address" => sub {
    my ( $fn );

    before all => sub {
        $fn = \&ITOne::Parser::cleanup_address;
    };

    it "should return cleaned address" => sub {
        my $address = q{:blackhole: <tpxmuwr@somehost.ru>};

        is $fn->( $address ), 'tpxmuwr@somehost.ru';
    };
};

runtests;
