BEGIN {
    use strict;
    use warnings;
    # XXX: хак с душком
    use Cwd;
    $ENV{MOJO_HOME} = Cwd::abs_path( $INC[0] . '/../' );
    require "$ENV{MOJO_HOME}/app.pl" unless $^C;
};

use utf8;
use strict;
use warnings;

use Test::Spec;
use Test::Mojo;

use ITOne::Model::Finder;

describe "search" => sub {
    my ( $t, $form, $entry );

    before all => sub {
        $t = Test::Mojo->new;

        $entry = {
            timestamp   => '2012-02-13 14:59:16',
            internal_id => '1Rwtcq-0000Ac-5W',
            logline     => 'grereyjbpddf@gmail.com R=dnslookup',
        };

        ITOne::Model::Finder->stubs(
            by_address => sub { ( 1, [ $entry ] ) },
        );
    };

    before each => sub {
        $form = {
            q => 'grereyjbpddf@gmail.com',
        };
    };

    it "should return found data" => sub {
        my $resp = $t->post_ok( "/search", form => $form )
          ->status_is( 200 )
          ->content_type_is( 'application/json;charset=UTF-8' )
          ->json_is( '/query' => $form->{q} )
          ->json_is( '/page' => 0 )
          ->json_is( '/limit' => 100 )
          ->json_is( '/total' => 1 )
          ->json_is( '/entries/0/timestamp' => $entry->{timestamp} )
          ->json_is( '/entries/0/internal_id' => $entry->{internal_id} )
          ->json_is( '/entries/0/logline' => $entry->{logline} );
    };

    it "should return empty data" => sub {
        ITOne::Model::Finder->stubs(
            by_address => sub { ( 0, [] ) },
        );

        my $resp = $t->post_ok( "/search", form => $form )
          ->status_is( 200 )
          ->content_type_is( 'application/json;charset=UTF-8' )
          ->json_is( '/query' => $form->{q} )
          ->json_is( '/page' => 0 )
          ->json_is( '/limit' => 100 )
          ->json_is( '/total' => 0 )
          ->json_is( '/entries' => [] );
    };

    it "should does not search w/o query" => sub {
        ITOne::Model::Finder->expects('by_address')->never;

        $form->{q} = undef;

        my $resp = $t->post_ok( "/search", form => $form )
          ->status_is( 200 )
          ->content_type_is( 'application/json;charset=UTF-8' )
          ->json_is( '/query' => undef )
          ->json_is( '/page' => 0 )
          ->json_is( '/limit' => 100 )
          ->json_is( '/total' => 0 )
          ->json_is( '/entries' => [] );
    };
};

runtests;
