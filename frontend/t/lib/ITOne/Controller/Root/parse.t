BEGIN {
    use strict;
    use warnings;
    # XXX: хак с душком
    use Cwd;
    $ENV{MOJO_HOME} = Cwd::abs_path( $INC[0] . '/../' );
    require "$ENV{MOJO_HOME}/app.pl" unless $^C;
};

use utf8;
use strict;
use warnings;

use Test::Spec;
use Test::Mojo;

use File::Basename;
use Mojo::JSON qw( true false );

use ITOne::Model::Uploader;

describe "parse" => sub {
    my ( $t, $form, $logsample, $sth );

    before all => sub {
        $t = Test::Mojo->new;

        $sth = stub(
            execute => 1,
            rows    => 1,
        );

        ITOne::Model::Uploader->stubs(
            stmt_handle => $sth,
        );

        $logsample = dirname( Cwd::abs_path( __FILE__ ) ) . '/sample.log';
    };

    before each => sub {
        $form = {
            maillog => {
                file => $logsample
            },
        };
    };

    it "should return success response" => sub {
        my $resp = $t->post_ok( "/parse", form => $form )
          ->status_is( 200 )
          ->content_type_is( 'application/json;charset=UTF-8' )
          ->json_is( '/success' => true )
          ->json_is( '/size' => 661 )
          ->json_is( '/name' => 'sample.log' )
          ->json_has( '/stats' )
          ->json_is( '/stats/messages' => 1 )
          ->json_is( '/stats/logs' => 3 );
    };

    it "should return unsuccess response" => sub {
        $form->{maillog} = undef;

        my $resp = $t->post_ok( "/parse", form => $form )
          ->status_is( 200 )
          ->content_type_is( 'application/json;charset=UTF-8' )
          ->json_is( '/success' => false )
          ->json_is( '/size' => 0 )
          ->json_is( '/name' => 'N/A' )
          ->json_has( '/stats' )
          ->json_is( '/stats/messages' => 0 )
          ->json_is( '/stats/logs' => 0 );
    };
};

runtests;
