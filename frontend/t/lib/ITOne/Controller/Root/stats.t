BEGIN {
    use strict;
    use warnings;
    # XXX: хак с душком
    use Cwd;
    $ENV{MOJO_HOME} = Cwd::abs_path( $INC[0] . '/../' );
    require "$ENV{MOJO_HOME}/app.pl" unless $^C;
};

use utf8;
use strict;
use warnings;

use Test::Spec;
use Test::Mojo;

use Mojo::JSON qw( true );

use ITOne::Model::Finder;

describe "stats" => sub {
    my ( $t, $form );

    before all => sub {
        $t = Test::Mojo->new;

        ITOne::Model::Finder->stubs(
            entries_count => {
                success  => true,
                messages => 42,
                logs     => 43,
            },
        );
    };

    before each => sub {
        $form = {};
    };

    it "should return result" => sub {
        my $resp = $t->post_ok( "/stats", form => $form )
          ->status_is( 200 )
          ->content_type_is( 'application/json;charset=UTF-8' )
          ->json_is( '/success' => true )
          ->json_is( '/messages' => 42 )
          ->json_is( '/logs' => 43 );
    };
};

runtests;
