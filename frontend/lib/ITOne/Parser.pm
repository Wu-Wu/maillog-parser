package ITOne::Parser;

=encoding utf8

=head1 NAME

ITOne::Parser

=head1 DESCRIPTION

Разбор строки C<maillog> и формирование данные для заливки записи в таблицу.

=head1 SYNOPSYS

    use ITOne::Parser;

    my $parser = ITOne::Parser->new;

    my $entry = $parser->parse( $line );

=head1 AUTHOR

Anton Gerasimov <me@zyxmasta.com>

=cut

use utf8;
use strict;
use warnings;
use v5.20.0;

use Readonly;
use POSIX ();

Readonly my %FLAG_HANDLERS => (
    # прибытие сообщения
    '<='       => \&gather_message_id,
    # нормальная доставка сообщения
    '=>'       => \&gather_address,
    # доставка не удалась
    '**'       => \&gather_address,
    # доставка задержана
    '=='       => \&gather_address,
    # дополнительный адрес в той же доставке
    '->'       => \&gather_address,
    # обработчик по умолчанию
    '_default' => \&gather_string,
);

=head1 METHODS

=cut

=head2 new

Конструктор экземпляра парсера.

    ITOne::Parser->new;

Возвращается экземпляр объекта парсера.

=cut

sub new {
    my ( $self ) = @_;

    my $class = ref $self || $self;

    return bless {}, $class;
}

=head2 parse

=cut

sub parse {
    my ( $self, $line ) = @_;

    return      unless $line;

    my ( $date, $time, $internal_id, $flag, $extra ) = split /\s/ => $line, 5;

    # обработчик данных
    my $handler = $FLAG_HANDLERS{$flag} || $FLAG_HANDLERS{_default};

    if ( my @data = $handler->( $self, $flag, $extra ) ) {
        # есть какие-то данные
        return {
            destination => $flag eq '<=' ? 'messages' : 'logs',
            created_at  => join( ' ' => $date, $time ),
            internal_id => $internal_id,
            logline     => $extra,
            # вливаем данные из обработчика
            @data,
        };
    }

    # нет записи
    return;
}

=head2 gather_message_id

Получение идентификатора сообщения из строки.

    $instance->gather_message_id( $flag, $extra );

где

=over 4

=item B<flag> - флаг сообщения (Scalar);

=item B<extra> - дополнительные данные из лога (Scalar);

=back

Возврщает список (Array) с идентификатором сообщения, либо C<undef>.

=cut

sub gather_message_id {
    my ( $self, $flag, $extra ) = @_;

    return  unless $extra;

    my @tokens = tokenize( $extra )->@*;

    shift( @tokens );

    if ( my $msg_id = { @tokens }->{id} ) {
        return (
            message_id => $msg_id,
        );
    }

    # XXX: нет идентификатора
    return;
}

=head2 gather_address

Получение почтового адреса строки.

    $instance->gather_address( $flag, $extra );

где

=over 4

=item B<flag> - флаг сообщения (Scalar);

=item B<extra> - дополнительные данные из лога (Scalar);

=back

Возврщает список (Array) с адресом отправителя/получателя.

=cut

sub gather_address {
    return  unless $_[2];

    return (
        address => cleanup_address( shift( tokenize( $_[2] )->@* ) ),
    );
}

=head2 gather_string

Получение заглушки с данным для служебных сообщений.

    $instance->gather_string( $flag, $extra );

где

=over 4

=item B<flag> - флаг сообщения (Scalar);

=item B<extra> - дополнительные данные из лога (Scalar);

=back

Возврщает список (Array) с пустым адресом отправителя/получателя и строкой
лога без временной метки.

=cut

sub gather_string {
    return  unless $_[1];

    return (
        address => '',
        logline => $_[1],
    );
}

=head1 FUNCTIONS

=cut

=head2 tokenize

Токенизация строки лога для последующего разбора данных.

    tokenize( $string );

Возвращает список (ArrayRef) элементов.

=cut

sub tokenize {
    return [
        map { join '' => reverse split // => $_ }
        map { reverse split /\s/ => $_, 2 }
        map { join '' => reverse split // => $_ }
        split /=/ => shift,
    ];
}

=head2 cleanup_address

Очистка почтового адреса от лишних символов.

    cleanup_address( $string );

Возвращает очищенное значение почтового адреса (Scalar).

=cut

sub cleanup_address {
    my ( $source ) = @_;

    my ( $address ) = $source =~ m|([\w\.]+@[\w\.]+)|;

    return $address;
}

1;
