package ITOne::Model::Finder;

=encoding utf8

=head1 NAME

ITOne::Model::Finder

=head1 DESCRIPTION

Модель для поиска данных.

=head1 AUTHOR

Anton Gerasimov <me@zyxmasta.com>

=cut

use utf8;
use strict;
use warnings;

use Mojo::JSON qw( true false );

use parent 'ITOne::Model::Base';

=head1 METHODS

=cut

=head2 by_address

Поиск данных по почтовому адресу.

    $instance->by_address( $address, $page, $limit );

Параметры:

=over 4

=item B<address> - почтовый адрес (Scalar);

=item B<page> - номер страницы результатов (Scalar);

=item B<limit> - количество записей на страницу (Scalar);

=back

Возвращает список (Array) из общего числа записей и ArrayRef-Of-HashRefs
выборки в переделах лимита.

    [
        {
            timestamp   => ..,
            internal_id => ..,
            logline     => ..,
        },
    ];

=cut

sub by_address {
    my ( $self, $address, $page, $limit ) = @_;

    # TODO: $page, $limit

    my $entries = $self->db->selectall_arrayref(
        q{
            SELECT SQL_CALC_FOUND_ROWS
                m.created_at AS timestamp,
                l.internal_id,
                l.logline
            FROM
                logs l
            JOIN
                messages m ON l.internal_id = m.internal_id
            WHERE
                l.address = ?
            ORDER BY
                l.internal_id,
                l.created_at
            LIMIT
                ?
        },
        { Slice => {} },
        $address,
        $limit,
    );

    my $total = $self->db->selectrow_array( 'SELECT FOUND_ROWS()' );

    return ( $total, $entries );
}

=head2 entries_count

Статистика по количеству записей в таблицах.

    $instance->entries_count();

Возвращает HashRefs:

    {
        success  => true,
        messages => ..,     # кол-во строк в messages
        logs     => ..,     # кол-во строк в logs
        broken   => false,  # если в обоих таблицах есть записи
    };

=cut

sub entries_count {
    my ( $self ) = @_;

    my ( $messages_rows ) = $self->db->selectrow_array(
        q{ SELECT COUNT(*) FROM messages },
    );

    my ( $logs_rows ) = $self->db->selectrow_array(
        q{ SELECT COUNT(*) FROM logs },
    );

    return {
        success  => true,
        messages => $messages_rows,
        logs     => $logs_rows,
        broken   => $messages_rows && $logs_rows ? false : true,
    };
}

1;
