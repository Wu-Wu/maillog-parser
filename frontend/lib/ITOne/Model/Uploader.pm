package ITOne::Model::Uploader;

=encoding utf8

=head1 NAME

ITOne::Model::Uploader

=head1 DESCRIPTION

Модель для манипуляции данными в базе.

=head1 AUTHOR

Anton Gerasimov <me@zyxmasta.com>

=cut

use utf8;
use strict;
use warnings;
use Carp qw( confess );

use Mojo::JSON qw( true false );
use ITOne::Constant;

use parent 'ITOne::Model::Base';

my $_stmt = {};

=head1 METHODS

=cut

=head2 flush_db

Очистка таблиц базы от всех записей.

    $instance->flush_db();

Возвращает HashRef вида:

    {
        success  => true,
        messages => ..,   # кол-во удалённых строк
        logs     => ..,   # тоже самое
    };

=cut

sub flush_db {
    my ( $self ) = @_;

    return {
        success  => true,
        messages => 0 + $self->db->do(q{ DELETE FROM messages }),
        logs     => 0 + $self->db->do(q{ DELETE FROM logs }),
    };
}

=head2 stmt_handle

Получить хэндл оператора для добавления данных в таблицу C<messages>
или C<logs>.

    $instance->stmt_handle( $name );

где B<name> - идентификатор хэндла (Scalar).

Возвращает экземпляр объекта L<DBI::st>.

=cut

sub stmt_handle {
    my ( $self, $name ) = @_;

    confess "Invalid handle name: '${name}'"
        unless exists $ITOne::Constant::QUERY_ATTRS{$name};

    # XXX: хэндл уже запрашивали (возвращаем значение)
    return $_stmt->{$name} if $_stmt->{$name};

    my @attrs = $ITOne::Constant::QUERY_ATTRS{$name}->@*;

    my $query =
        "INSERT INTO ${name} (" . join( ', ' => @attrs ) . ") " .
        "VALUES (" . join( ', ' => map { '?' } @attrs ) . ")";

    # формируем хэндл и кэшируем его для последующего использования
    return $_stmt->{$name} = $self->db->prepare( $query );
}

=head2 insert_entry

Добавить строку с данными в таблицу C<messages> или C<logs>.

    $instance->insert_entry( $entry );

где B<entry> - добавляемые атрибуты (HashRef).

Возвращает список (Array) из имени таблицы и количества добавленных строк.

=cut

sub insert_entry {
    my ( $self, $entry ) = @_;

    my $destination = delete $entry->{destination};

    my $st = $self->stmt_handle( $destination );

    $st->execute(
        @$entry{ $ITOne::Constant::QUERY_ATTRS{$destination}->@* },
    );

    return ( $destination, $st->rows );
}

1;
