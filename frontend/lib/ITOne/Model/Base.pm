package ITOne::Model::Base;

=encoding utf8

=head1 NAME

ITOne::Model::Base

=head1 DESCRIPTION

Базовая модель.

=head1 AUTHOR

Anton Gerasimov <me@zyxmasta.com>

=cut

use utf8;
use strict;
use warnings;

use parent 'Class::Accessor::Fast';

my @fields = qw( db );

__PACKAGE__->mk_accessors( @fields );

=head1 METHODS

=cut

=head2 new

Конструктор модели

=cut

sub new {
    my ( $self, %args ) = @_;

    my $class = ref $self || $self;

    return bless { %args }, $class;
}

1;
