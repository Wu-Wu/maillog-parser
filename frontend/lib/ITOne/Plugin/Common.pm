package ITOne::Plugin::Common;

use strict;
use warnings;

use Mojo::Base qw( Mojolicious::Plugin );

sub register {
    my ( $self, $app ) = @_;

    $app->routes->namespaces( [qw( ITOne::Controller )] );

    $app->plugin( $_ ) for qw( BaseRoutes Helpers );
}

1;
