package ITOne::Plugin::Helpers;

=encoding utf8

=head1 NAME

ITOne::Plugin::Helpers

=head1 DESCRIPTION

Плагин хелперов.

=head

=head1 AUTHOR

Anton Gerasimov <me@zyxmasta.com>

=cut

use utf8;
use strict;
use warnings;

use Carp qw( confess );
use URI;
use DBI;

use Mojo::Base qw( Mojolicious::Plugin );
use Mojo::Loader qw( load_class );

my $_db;

sub register {
    my ( $self, $app ) = @_;

    $app->helper( db => \&db_instance );
    $app->helper( model => \&model_instance );
}

=head1 HELPERS

=cut

=head2 db_instance

Возвращает инстанс базы данных.

    $c->db();

=cut

sub db_instance {
    unless ( $_db ) {
        my $database_url = $ENV{DATABASE_URL};

        confess "Misconfigured" unless $database_url;

        $database_url =~ s/^mysql://;

        my $url = URI->new( $database_url => 'http' );

        my ( $username, $password ) = split /:/ => $url->userinfo;

        my %dsn = (
            host     => $url->host,
            port     => $url->port,
            database => substr $url->path, 1,
        );

        my $dsn = 'DBI:mysql:' . join(
            ';' => map { $_ . '=' . $dsn{$_} } keys %dsn
        );

        $_db = DBI->connect(
            $dsn, $username, $password,
            {
                RaiseError => 1,
                PrintError => 1,
                AutoCommit => 1,
            },
        );
    }

    return $_db;
}

=head2 model_instance

Возвращает инстанс модели.

    $c->model( $name );

=cut

sub model_instance {
    my ( $c, $name ) = @_;

    my $model = "ITOne::Model::${name}";

    if ( my $ex = load_class $model ) {
        confess ref $ex ? "Exception: $ex" : 'Not found!';
    }

    return $model->new( db => $c->db );
}

1;
