package ITOne::Plugin::BaseRoutes;

use strict;
use warnings;
use Mojo::Base qw( Mojolicious::Plugin );

sub register {
    my ( $self, $app ) = @_;

    my $r = $app->routes;

    $r->get( '/' )->to( cb => sub {
        shift->render( template => 'index' );
    });

    # XXX: health-check
    $r->get( '/ping' => sub {
        shift->render( text => 'PONG' );
    });
}

1;
