package ITOne::Constant;

=encoding utf8

=head1 NAME

ITOne::Constant

=head1 DESCRIPTION

Константы.

=head1 SYNOPSYS

    use ITOne::Constant;
    ...

    say $ITOne::Constant::DEFAULT_LIMIT;

=head1 AUTHOR

Anton Gerasimov <me@zyxmasta.com>

=cut

use utf8;
use strict;
use warnings;

use Readonly;

=head1 CONSTANTS

=cut

=head2 $DEFAULT_LIMIT

Лимит количества записей по умолчанию.

=cut

Readonly our $DEFAULT_LIMIT => 100;

=head2 %QUERY_ATTRS

Аттрибуты таблиц с данными.

=cut

Readonly our %QUERY_ATTRS => (
    messages => [qw( created_at internal_id message_id logline )],
    logs     => [qw( created_at internal_id logline address )],
);

1;
