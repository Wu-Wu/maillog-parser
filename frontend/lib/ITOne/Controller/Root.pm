package ITOne::Controller::Root;

=encoding utf8

=head1 NAME

ITOne::Controller::Root

=head1 DESCRIPTION

Контроллер обработки запросов.

=head1 AUTHOR

Anton Gerasimov <me@zyxmasta.com>

=cut

use utf8;
use strict;
use warnings;

use Mojo::Base 'Mojolicious::Controller';
use Mojo::JSON qw( true false );

use ITOne::Constant;
use ITOne::Parser;

=head1 ACTIONS

=cut

=head2 search

Поиск информации в базе.

=cut

sub search {
    my ( $c ) = @_;

    my $entries = [];

    my $term  = $c->req->param('q');
    my $page  = $c->req->param('page')  || 0;
    my $limit = $c->req->param('limit') || $ITOne::Constant::DEFAULT_LIMIT;
    my $total = 0;

    if ( $term ) {
        ( $total, $entries ) = $c->model('Finder')->by_address(
            $term,
            $page,
            $limit,
        );
    }

    $c->render(
        json => {
            query   => $term,
            entries => $entries,
            page    => $page,
            limit   => $limit,
            total   => $total,
        },
    );
}

=head2 parse

Парсинг лога и заливка данных в таблицы.

=cut

sub parse {
    my ( $c ) = @_;

    my ( $filesize, $filename ) = ( 0, 'N/A' );

    my $success = false;
    my $stats = {
        messages => 0,
        logs     => 0,
    };

    if ( my $logfile = $c->req->upload('maillog') ) {
        $filesize = $logfile->size;
        $filename = $logfile->filename;

        my @lines = split /\n/ => $logfile->asset->slurp;
        my $parser = ITOne::Parser->new;

        my $model = $c->model('Uploader');

        while ( my $line = shift( @lines ) ) {
            next    if $line =~ /^$/;

            if ( my $entry = $parser->parse( $line ) ) {
                my ( $destination, $rows ) = $model->insert_entry( $entry );

                $stats->{$destination} += $rows;
            }
        }

        $success = true;
    }

    $c->render(
        json => {
            success => $success,
            size    => $filesize,
            name    => $filename,
            stats   => $stats,
        },
    );
}

=head2 truncate

Очистка таблиц лога от записей.

=cut

sub truncate {
    my ( $c ) = @_;

    $c->render(
        json => $c->model('Uploader')->flush_db(),
    );
}

=head2 stats

Статистика записей в таблицах.

=cut

sub stats {
    my ( $c ) = @_;

    $c->render(
        json => $c->model('Finder')->entries_count(),
    );
}

1;
