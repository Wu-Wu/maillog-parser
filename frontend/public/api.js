/*
 * получение статистики базы данных
 */
async function getStatistics() {
  try {
    const { data } = await axios.post('/stats');
    document.getElementById('messages-count').textContent = data.messages;
    document.getElementById('logs-count').textContent = data.logs;
  } catch (error) {
    console.error(error);
  }
}

window.onload = function () {
    document.getElementById('results').style.visibility = 'hidden';
    document.getElementById('nothing-found').style.visibility = 'hidden';

    getStatistics();
};

/*
 * поиск данных в базе
 */
async function searchEmail() {
  try {
    const query = document.getElementById('search-query');
    const overlimit = document.getElementById('overlimit');
    const found = document.getElementById('found-entries');
    const results = document.getElementById('results');
    const nothing = document.getElementById('nothing-found');

    nothing.style.visibility = 'hidden';
    results.style.visibility = 'hidden';
    found.innerHTML = '';
    overlimit.innerHTML = '';

    const formData = new FormData();
    formData.append("q", query.value);
    const { data } = await axios.post('/search', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    });

    if ( data.total > data.limit ) {
      // показываем информацию о наличии записей сверхлимита
      overlimit.innerHTML = 'показано ' + data.limit + ' из ' + data.total;
    }

    if ( data.entries.length ) {
      found.innerHTML = tmpl('search-result-items', data);
      results.style.visibility = 'visible';
    } else {
      nothing.style.visibility = 'visible';
    }
  } catch (error) {
    console.error(error);
  }
}

/*
 * очистка базы от данных
 */
async function truncateDatabase() {
  var message;
  var style;
  const ident = makeIdent();

  try {
    this.disabled = true;
    const caption = this.innerText;
    this.innerText = "Очищаем..";
    const { data } = await axios.post('/truncate');
    message = "Очистка таблиц: " + ( data.success ? 'успешна' : 'не успешна' );
    style = "bg-success";
    this.disabled = false;
    this.innerText = caption;
  } catch (error) {
    message = error;
    style = "bg-danger";
  }

  toast(ident, tmpl('toast-alert', {
    ident: ident,
    style: style,
    message: message
  }));

  await getStatistics();
}

/*
 * загрузка файлу для парсинга данных
 */
async function uploadMaillog() {
  try {
    this.disabled = true;
    const caption = this.innerText;
    this.innerText = "Загружаем..";
    const logfile = document.querySelector('#maillog-source');
    const formData = new FormData();
    formData.append("maillog", logfile.files[0]);
    const { data } = await axios.post('/parse', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    });
    this.disabled = false;
    this.innerText = caption;
  } catch (error) {
    console.error(error);
  }

  await getStatistics();
}

/*
 * отображение уведомлений
 */
function toast (ident, content) {
  document.getElementById('toasts').innerHTML = content;
  const toast = new bootstrap.Toast(document.getElementById(ident));
  toast.show();
}

/*
 * создание "случайного" идентификатора
 */
function makeIdent () {
  var ident    = 'ident-';
  const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const length   = alphabet.length;
  for ( var i = 0; i < 15; i++ ) {
    ident += alphabet.charAt(Math.floor(Math.random() * length));
  }
  return ident;
}

/*
 * установка обработчиков событий для кнопок
 */
const searchButton = document.getElementById('search-button');
searchButton.addEventListener("click", searchEmail);

const truncateButton = document.getElementById('truncate-button');
truncateButton.addEventListener("click", truncateDatabase);

const uploadButton = document.getElementById('upload-button');
uploadButton.addEventListener("click", uploadMaillog);
