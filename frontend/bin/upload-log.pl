#!/usr/bin/env perl

use strict;
use warnings;
use lib::abs qw( ../lib );
use v5.10.1;

use POSIX qw( strftime );

use ITOne::Parser;
use ITOne::Plugin::Helpers;
use ITOne::Model::Uploader;

$| = 1;

my $stats = {
    messages => 0,
    logs     => 0,
};

my $model = ITOne::Model::Uploader->new(
    db => ITOne::Plugin::Helpers::db_instance(),
);

my $parser = ITOne::Parser->new;

log_message( 'Upload STDIN to database...' );

while ( my $line = <> ) {
    chomp($line);
    next    if $line =~ /^$/;

    if ( my $entry = $parser->parse( $line ) ) {
        my ( $destination, $rows ) = $model->insert_entry( $entry );

        $stats->{$destination} += $rows;
    }
}

log_message(
    sprintf(
        'messages: %s row(s); logs: %s row(s)' =>
        @$stats{qw( messages logs )}
    )
);

log_message( 'Done!' );

sub log_message {
    my ( $message ) = @_;

    my $at = strftime "%F %T", localtime time;

    say join( ' ' => $at, $message );
}

1;
