## Разбор и поиск по логу почты

Парсер `maillog` и поиска записей по `email`.

### Запуск проекта

Для запуска проекте необходим докер.

### Алгоритм запуска

Клонировать репозиторий:

    $ git clone git@bitbucket.org:Wu-Wu/maillog-parser.git

Перейти в каталог проекта:

    $ cd maillog-parser

Выполнить команду:

    $ docker-compose up

Далее минут 10-15 (в зависимости от ресурсов машины и сети) можно своими
делами.

После сборки образов переходим в браузер по адресу

    http://localhost:9000

будет виден интерфейс проекта

![Основной экран](images/img01.png "Основной экран")

### Основные моменты работы

#### Загрузка файла для парсинга

Можно использовать интерфейс для загрузки. В блоке "Данные в таблицах",
выбирается файл и нажимается кнопка "Загрузить". Подходит для небольших файлов.

![Загрузка лога](images/img02.png "Загрузка лога")

После загрузки и разбора файла, автоматически обновится статистика.

![Статистика](images/img03.png "Статистика")

Альтернативный способ загрузки файла лога и его разбора:

    $ cat mail.log | docker-compose exec -T itone-frontend /app_from_host/bin/upload-log.pl

В результате в консоли будет что-то вроде:

    2021-11-25 13:41:21 Upload STDIN to database...
    2021-11-25 13:41:44 messages: 1561 row(s); logs: 8079 row(s)
    2021-11-25 13:41:44 Done!

#### Очистка таблиц от данных

Выполняется по нажатию кнопки "Очистить".

![Очистка данных](images/img04.png "Очистка данных")

После очистки автоматически обновится статистика.

Альтернативный способ очистки (через клиент mysql):

    $ docker exec -ti itone-mariadb mysql -h localhost -u USERNAME -p DATABASE

Параметры `USERNAME`, `DATABASE` и пароль можно взять из конфига `docker-compose.yml`

После успешного ввода пароля выполнить очистку данных:

    MariaDB [maillog]>TRUNCATE TABLE logs;
    MariaDB [maillog]>TRUNCATE TABLE messages;

#### Поиск данных

Для поиска данных, ввести значение в поле "Почтовый адрес" и нажать кнопку "Найти".
Будут выведены результаты, если таковые имеются. Если результатов больше 100, то будет
указано сколько их.

![Поиск данных](images/img05.png "Поиск данных")

Если ничего не найдено, то будет выведено соттветствующее сообщение.

![Ничего не найдено](images/img06.png "Ничего не найдено")

### Завершение работы

Для завершения работы необходимо выполнить нажать кнопку `Ctrl+C` в консоли,
где запущен процесс `docker-compose up`.

### Прогон тестов

Запуск тестов можно осуществить выполнив команду в консоли

    $ docker exec -ti itone-frontend bash -c "cd /app_from_host && prove -rv -I/app_from_host/lib t/"

на экране будет нечто подобное

    t/lib/ITOne/Model/Base/new.t ..............
    ok 1 - new should create new instance
    ok 2 - new should inject params
    ok 3 - new should update accessor data
    1..3
    ok
    t/lib/ITOne/Model/Finder/by_address.t .....
    ok 1 - by_address always should invoke db instance
    ok 2 - by_address always should invoke selectall_arrayref
    ok 3 - by_address always should invoke selectrow_array
    ok 4 - by_address always should return results
    ok 5 - by_address get entries should match query
    ok 6 - by_address get entries should match attrs
    ok 7 - by_address get entries should match bind values
    ok 8 - by_address get total should match query
    1..8
    ok
    t/lib/ITOne/Model/Finder/entries_count.t ..
    ok 1 - entries_count always should invoke db instance
    ok 2 - entries_count always should invoke selectrow_array
    ok 3 - entries_count get data should match queries
    ok 4 - entries_count results should always be
    ok 5 - entries_count results should set broken to 'false' (no messages)
    ok 6 - entries_count results should set broken to 'false' (no logs)
    ok 7 - entries_count results should set broken to 'true' otherwise
    1..7
    ok
    All tests successful.
    Files=3, Tests=18,  3 wallclock secs ( 0.04 usr  0.03 sys +  0.57 cusr  0.15 csys =  0.79 CPU)
    Result: PASS
