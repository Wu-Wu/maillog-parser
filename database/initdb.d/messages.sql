USE `maillog`;

CREATE TABLE `messages` (
    `created_at`    TIMESTAMP       NOT NULL,
    `internal_id`   CHAR(16)        NOT NULL,
    `message_id`    VARCHAR(256)    NOT NULL,
    `logline`       TEXT            NOT NULL,
    PRIMARY KEY (`message_id`)
) ENGINE=InnoDB;

CREATE INDEX `created_at_idx` ON `messages` (`created_at`);
CREATE INDEX `internal_id_idx` ON `messages` (`internal_id`);
