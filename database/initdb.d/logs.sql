USE `maillog`;

CREATE TABLE `logs` (
    `created_at`    TIMESTAMP       NOT NULL,
    `internal_id`   CHAR(16)        NOT NULL,
    `logline`       TEXT            NOT NULL,
    `address`       VARCHAR(256)    NOT NULL
) ENGINE=InnoDB;

CREATE INDEX `address_idx` ON `logs` (`address`);
